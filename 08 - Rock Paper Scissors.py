class player:
	chose = ""
	win_count = 0
	lose_count = 0
	turns = 0

	def plays(self, play):
		self.chose = play
	def add_win_count(self):
		self.win_count += 1
	def add_lose_count(self):
		self.lose_count +=1
	def add_turns(self):
		self.turns += 1
	def empty_chose(self):
		self.chose = ""

def compare_choices(player1, player2):
	results = []
	player1wins = False
	tie = False

	if player1.chose == player2.chose:
		tie = True
		
	if player1.chose == options[0] and player2.chose == options[1]:
		player1.add_lose_count()
		player2.add_win_count()
		
	if player1.chose == options[0] and player2.chose == options[2]:
		player1.add_win_count()
		player2.add_lose_count()
		player1wins = True
		
	if player1.chose == options[1] and player2.chose == options[0]:
		player1.add_win_count()
		player2.add_lose_count()
		player1wins = True
		
	if player1.chose == options[1] and player2.chose == options[2]:
		player1.add_lose_count()
		player2.add_win_count()
		
	if player1.chose == options[2] and player2.chose == options[0]:
		player1.add_lose_count()
		player2.add_win_count()

	if player1.chose == options[2] and player2.chose == options[1]:
		player1.add_win_count()
		player2.add_lose_count()
		player1wins = True

	results.append(player1wins)
	results.append(tie)
	return results

player1 = player()
player2 = player()
options = ["rock", "paper", "scissors", "quit"]

print("Rock Paper Scissors")
print("Type quit to quit\n")
while(True):

	try:	
		
		choice = input("Choose: ").lower()

		if choice not in options:
			print("Not one of the options!")
		else:
			if choice == options[3]:
				break

			if player1.turns % 2 == 0:
				player1.plays(choice)
			else:
				player2.plays(choice)	
			player1.add_turns()
			if player1.chose != "" and player2.chose != "":
				results = compare_choices(player1, player2)
				if results[1] == True:
					print("It's a tie!")
				elif results[0] == True:
					print("Player 1 wins!")
				else:
					print("Player 2 wins!")
				player1.empty_chose()
				player2.empty_chose()
				print("\n")
	except ValueError:
		print("Not a string!")
