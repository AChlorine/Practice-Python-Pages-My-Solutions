import datetime

def calculate_date_of_100th_birthday(age, current_year):
  date_of_birth = current_year - age
  date_of_100th_birthday = date_of_birth + 100
  return date_of_100th_birthday
def print_results(name, age, year_of_100th_birthday):
  print("Hello, " + name + ", you said you were " + str(age) + " years old, so you will turn 100 in the year " + str(year_of_100th_birthday) + ".")

current_date = datetime.datetime.now()
current_year = current_date.year

name = input("What is your name: ")
age = int(input("What is your age: "))
date_of_100th_birthday = calculate_date_of_100th_birthday(age, current_year)
print_results(name, age, date_of_100th_birthday)