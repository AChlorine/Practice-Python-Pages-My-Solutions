divisors = range(2, 11)
result = []
try:
  number = int(input("Type a number: "))
  for item in divisors:
    if number % item == 0:
      result.append(item)
  print("Result: " + str(result))
except ValueError:
  print("Not a number!")