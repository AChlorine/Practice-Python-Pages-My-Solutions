import random
class player:
    number_of_guesses = 0

    def add_number_of_guesses(self):
        self.number_of_guesses += 1
        
def check_if_within_bounds(choice):
    within_bounds = False
    if choice > 9:
        print("Number is higher than 9!")
    elif choice <= 0:
        print("Number is lower than 1!")
    else:
        within_bounds = True
    return within_bounds

print("Guess a number from 1 to 9\n")
number_to_guess = random.randint(1,10)
player = player()

while(True):
    try:
        choice = int(input("Type a number from 1 to 9: "))
        within_bounds = check_if_within_bounds(choice)
        if within_bounds == True:
            if choice == number_to_guess:
                print("You guessed the right number!")
                print("It took you " + str(player.number_of_guesses) + " attempts!")
                break
            elif choice > number_to_guess:
                print("You guessed too high!")
                player.add_number_of_guesses()
            elif choice < number_to_guess:
                print("You guessed too low!")
                player.add_number_of_guesses()
            
    except ValueError:
        print("Not a number!")
        player.add_number_of_guesses()
