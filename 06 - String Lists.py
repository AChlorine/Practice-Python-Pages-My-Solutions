
try:
    string = input("Type a string: ")
    placeholder = string.replace(" ","").lower()
    reverse = placeholder[::-1]
    if placeholder == reverse:
        print(string + " is a palindrome!")
    else:
        print(string + " is not a palindrome!")
except ValueError:
    print("Not a string!")
