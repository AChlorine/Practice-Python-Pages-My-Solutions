def is_odd(dividend):
  is_odd = True
  
  if dividend % 2 == 0:
    is_odd = False

  return is_odd

def is_multiple_of_four(dividend):
  is_multiple_of_four = False

  if dividend % 4 == 0:
    is_multiple_of_four = True
  return is_multiple_of_four

def is_evenly_divided_by_divisor(dividend, divisor):
  is_evenly_divided_by_divisor = False
  
  if dividend % divisor == 0:
    is_evenly_divided_by_divisor = True
  
  return is_evenly_divided_by_divisor

def print_results(is_odd, is_multiple_of_four, is_evenly_divided_by_divisor):
  if is_odd == True:
    print("Dividend is odd!")
  else:
    print("Dividend is even!")
  
  if is_multiple_of_four == True:
    print("Dividend is also a multiple of four!")
  else: 
    print("Dividend is also not a multiple of four!")
  
  if is_evenly_divided_by_divisor == True:
    print("Finally, dividend is divided evenly by the divisor!")
  else:
    print("Finally, dividend is not divided evenly by the divisor!")
try:
  dividend = int(input("Type a dividend: "))
  divisor = int(input("Type a divisor: "))
  is_odd = is_odd(dividend)
  is_multiple_of_four = is_multiple_of_four(dividend)
  is_evenly_divided_by_divisor = is_evenly_divided_by_divisor(dividend, divisor)
  print_results(is_odd, is_multiple_of_four, is_evenly_divided_by_divisor)
  
except ValueError:
  print("Not a number!")

