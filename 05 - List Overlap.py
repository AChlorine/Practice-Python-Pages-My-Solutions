import random

def randomize(size_of_a, size_of_b):
  a = []
  b = []
  result = []

  for i in range(0, size_of_a):
    a.append(random.randint(0, 50))
  for i in range(0, size_of_b):
    b.append(random.randint(0,50))
  result.append(a)
  result.append(b)
  return result

result = randomize(3, 7)
a = result[0]
b = result[1]
common = []

for item in a:
  if item in b and item not in common:
    common.append(item)

print(a)
print(b)
if len(common) == 0:
  print("No matches found.")
else:
  for item in common:
    print(item)



