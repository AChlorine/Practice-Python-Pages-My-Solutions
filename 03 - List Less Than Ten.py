a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
result = []
try:
  number_to_compare = int(input("Number to compare: "))
  for item in a:
    if item < number_to_compare:
      result.append(item)

except ValueError:
  print("Not a number!")

finally:
  if len(result) != 0:
    print("Original list: " + str(a))
    print("Results:")
    for item in result:
      print(item)